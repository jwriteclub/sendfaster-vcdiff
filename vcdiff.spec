Name:    vcdiff
Version: 0.8.5dev
Release: 1%{?dist}
Summary: VCDiff (open-vcdiff) provides an encoder and decoder based on the VCDIFF format (RFC 3284).

Group:   Utilities
License: Apache 2.0
URL: https://github.com/google/open-vcdiff
Source0: https://github.com/google/open-vcdiff/archive/%{name}-%{version}.tar.gz
BuildRequires: prelink
BuildRequires: flex, bison, llvm

%description
VCDIff (open-vcdiff on github) provides the reference implementation of the VCDIFF open
protocol for computing and decoding deltas over arbitrary data.

%prep
%setup -q %{name}-%{version}

%build
export CC="clang"
export CXX="clang++"
#export CFLAGS="-O3"
#export CXXFLAGS="-03"
./configure --enable-static --disable-shared
make %{?_smp_mflags}
clang++ -DNDEBUG -DNO_THREADS -DGTEST_HAS_TR1_TUPLE=0 -Wall -Wwrite-strings -Woverloaded-virtual -W -g -O2 -static --std=c++11 -stdlib=libstdc++ -lstdc++ -o vcdiff vcdiff_main.o ./.libs/libvcddec.a ./.libs/libvcdenc.a /home/centos/rpmbuild/BUILD/vcdiff-0.8.5dev/.libs/libvcdcom.a ./.libs/libgflags.a

%install
rm -rf %{buildroot}

# binary
mkdir -p %{buildroot}%{_bindir}
%{__install} -p -D -m 0755 vcdiff %{buildroot}%{_bindir}/vcdiff

# man pages
mkdir -p %{buildroot}%{_mandir}/man1
install -p -D -m 0644 man/vcdiff.1 %{buildroot}%{_mandir}/man1

%clean
rm -rf %{buildroot}

%post
cat <<BANNER
-----------------------------------------
-        VCDiff  (open-vcdiff)          -
-----------------------------------------
-                                       -
- %{version}                            -
-         Built By SendFaster           -
- Contact support@sendfaster.com with   -
- packaging problems. For all other     -
- support, please see:                  -
- https://github.com/google/open-vcdiff -
-----------------------------------------
BANNER

%files
%defattr(-,root,root,-)
%{_bindir}/vcdiff
%{_mandir}/man1/*

%changelog

* Fri Dec 11 2015 Christopher O'Connell <jwriteclub@gmail.com>
- Spec file build for RPM based systems.
- 0.8.5dev
