#/bin/bash

NAME="vcdiff"
VERSION="0.8.5dev"
FILE="${NAME}-${VERSION}"

echo "Preparing to build VCDIFF $FILE"


if [ -f ~/rpmbuild/SPECS/${NAME}.spec ]; then
    echo "Removing old spec file"
    rm -f ~/rpmbuild/SPECS/${NAME}.spec
fi

if [ -f ~/rpmbuild/SOURCES/${FILE}.tar.gz ]; then
    echo "Removing old tarball"
    rm -f ~/rpmbuild/SOURCES/${FILE}.tar.gz
fi

echo "Checking out source .."
rm -rf ./open-vcdiff
git clone https://github.com/google/open-vcdiff.git
echo " .. Done"

echo "Fetching dependencies .."
cd ./open-vcdiff
./fetch_prereq.sh
cd ..
echo " .. Done"

echo -n "Generating tool files .."
cd ./open-vcdiff
./autogen.sh
cd ..
echo " .. Done"

echo -n "Injecting spec file .."
cp ./vcdiff.spec ./open-vcdiff/${NAME}.spec
echo ". Done"

echo -n "Creating tarball .."
mv ./open-vcdiff ./${FILE}
tar cf ~/rpmbuild/SOURCES/${FILE}.tar.gz ${FILE}/*
echo ". Done"

echo -n "Installing specfile .."
cp ./${FILE}/${NAME}.spec ~/rpmbuild/SPECS/
echo ". Done"

echo -n "Cleaning up .."
rm -rf ./${FILE}
echo ". Done"

echo "Execute"
echo "     rpmbuild -ba ~/rpmbuild/SPECS/${NAME}.spec"
echo "to complete the build."
